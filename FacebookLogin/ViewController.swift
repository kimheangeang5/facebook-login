//
//  ViewController.swift
//  FacebookLogin
//
//  Created by Kimheang on 11/29/18.
//  Copyright © 2018 Kimheang. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookShare
import FBSDKLoginKit
import FBSDKShareKit
import FBSDKCoreKit
import AccountKit
class ViewController: UIViewController,FBSDKLoginButtonDelegate{
    @IBOutlet weak var imageView: UIImageView!
    var _accountKit: AKFAccountKit!
    var user = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        //Account Kit
        if _accountKit == nil {
            _accountKit = AKFAccountKit(responseType: .accessToken)
        }
        
        let toolBarLogin = UIButton(frame: CGRect(x: 0, y: 50, width: view.bounds.width, height: 40))
        toolBarLogin.backgroundColor = .blue
        toolBarLogin.setTitle("Facebook Login", for: .normal)
        // Handle clicks on the button
        toolBarLogin.addTarget(self, action: #selector(loginButtonClicked), for: .touchUpInside)
        let login = FBSDKLoginButton()
        login.center = view.center
        login.delegate = self
        view.addSubview(toolBarLogin)
        view.addSubview(login)
    }
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result:
        FBSDKLoginManagerLoginResult!, error: Error!) {
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,email,picture.type(large)"]).start(completionHandler: { (connection, result, error) in
            print(result!)
            if let userInfo = result as? [String:Any]{
                let userID = userInfo["id"]
                let url = URL(string: "https://graph.facebook.com/\(userID!)/picture?type=large")
                print(url!)
                do{
                    let data = try Data(contentsOf: url!)
                    let userProfile = UIImage(data: data)
                    self.imageView.image = userProfile
                }
                catch let error {
                    print("This is my error \(error)")
                }
            }
            
        })
        
//
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("U logged out")
        self.imageView.image = nil
    }
    @objc func loginButtonClicked() {
        let loginManager = FBSDKLoginManager()
        
        loginManager.logIn(withReadPermissions: ["public_profile"], from: self) { (result, error) in
            print(result!)
        }
    }
   // For Accont Kit
    func prepareLoginViewController(loginViewController: AKFViewController) {
        loginViewController.delegate = self
        
        //Costumize the theme
        let theme:AKFTheme = AKFTheme.default()
        theme.headerBackgroundColor = UIColor(red: 0.325, green: 0.557, blue: 1, alpha: 1)
        theme.headerTextColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        theme.iconColor = UIColor(red: 0.325, green: 0.557, blue: 1, alpha: 1)
        theme.inputTextColor = UIColor(white: 0.4, alpha: 1.0)
        theme.statusBarStyle = .default
        theme.textColor = UIColor(white: 0.3, alpha: 1.0)
        theme.titleColor = UIColor(red: 0.247, green: 0.247, blue: 0.247, alpha: 1)
        loginViewController.setTheme(theme)
    }
    func loginWithPhone(){
        let inputState = UUID().uuidString
        let vc = (_accountKit?.viewControllerForPhoneLogin(with: nil, state: inputState))!
        vc.enableSendToFacebook = true
        self.prepareLoginViewController(loginViewController: vc)
        self.present(vc as UIViewController, animated: true, completion: nil)
    }
    func loginWithEmail() {
        let inputState = NSUUID().uuidString
        let vc = _accountKit!.viewControllerForEmailLogin(withEmail: nil, state: inputState)
        self.prepareLoginViewController(loginViewController: vc)
        self.present(vc as UIViewController, animated: true, completion: nil)
    }
    @IBAction func loginByPhone(_ sender: Any) {
        self.loginWithPhone()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if _accountKit?.currentAccessToken != nil{
            // if the user is already logged in, go to the main screen
            print("Already Logged in")
//            DispatchQueue.main.async(execute: {
//                self.performSegue(withIdentifier: "showDetails", sender: self)
//            })
        }
        else{
            // Show the login screen
        }
    }
}
extension ViewController: AKFViewControllerDelegate {
    func viewController(viewController: UIViewController!, didCompleteLoginWithAccessToken accessToken: AKFAccessToken!, state: String!) {
        print("did complete login with access token \(accessToken.tokenString) state \(state)")
    }
    
    // handle callback on successful login to show authorization code
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
        print("didCompleteLoginWithAuthorizationCode")
    }
    
    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)!) {
        // ... handle user cancellation of the login process ...
        print("viewControllerDidCancel")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didFailWithError error: Error!) {
        // ... implement appropriate error handling ...
        print("\(viewController) did fail with error: \(error.localizedDescription)")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
        print("didCompleteLoginWith")
    }
}
